Parallelization of the k-means clustering algorithm for clouds detection in optical satellite images
====================================================================================================

## Objectives

Due to restricted visibility time of remote sensing polar platforms from earth
reception station, only a limited number of images can be transmitted.
In the case of optical images, an in-board cloud cover detection module will
allow to transmit only useful (i.e. weakly cloudy) images.
In order to derive such a module, we propose a method to detect cloudy areas
from subsampled images. 

This was given as scholar project by Mr.Beaudoin Laurent and Ms.Avanthey Loïca
for the "Extraction d'information sur des images de télédétection" course, given
to EPITA SCIA 4th year. 

We chose to go further and we worked on a parallelized version.

## Usage

To compile this project just go in the directory SRC and type 'make'

Then to execute the project you must go in the directory EXE and here you
have different executable:

* imaProject.exe: launch a viewer to see the image and its segmentation
* imaProject_disp_par.exe: launch a viewer to see the image and its segmentation
  done in parallel
* imaProject_auto.exe: launch a program that gives you the percentage of cloud
  of each images in the directory (they must end with '.bmp')
* imaProject_auto_par.exe: same as imaProject_auto.exe but in parallel

If you want to distribute the calcul of imaProject_auto_par.exe
run it like this : 'mpirun -np 2 -hosts client,localhost ./imaProject_auto_par.exe'

## Cuda

A begining of CUDA implementation is available in CUDA root folder.
Unfortunately, we didn't knew that our GPU used with CUDA was to old to allows
Dynamic Parallelism (available with GPU with compute capabilities of 3.5 and our
is 2.1). In the report, We made a long part on the experiment and how we would
have done CUDA optimization with Dynamic Parallelism. As so we tried to
parallelize only two main function used in K-mean: Vectors classification and
Update of centroids. But we faced some memory error with cudaMemcpy so we
didn't succeed to implement both.
