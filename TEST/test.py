import subprocess, os

os.chdir("../EXE")
result = subprocess.run(['./imaProjet_auto.exe'], stdout=subprocess.PIPE)
res = result.stdout.decode('utf-8').split('\n')
pic = dict()
for elm in res[:-3]:
    elm_split = elm.split(" ")
    pic[elm_split[3]] = elm_split[5]
result = subprocess.run(['mpirun', '-np', '1', './imaProjet_par.exe'], stdout=subprocess.PIPE)
res = result.stdout.decode('utf-8').split('\n')
for elm in res[:-3]:
    elm_split = elm.split(" ")
    if pic[elm_split[3]] == elm_split[5]:
        print(elm_split[3] + "\033[92m OK \033[0m")
        del pic[elm_split[3]]
    else:
        print(elm_split[3] + "\033[91m NOK \033[0m")

if len(pic) > 0:
    for key in pic:
        print(key + "\033[91m NOK \033[0m")
