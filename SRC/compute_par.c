#include <stdio.h>
#include <stdlib.h>
#include <gtk/gtk.h>
#include <error.h>
#include <omp.h>
#include <xmmintrin.h>

int compare_function(const void *a,const void *b) {
  unsigned char *x = (unsigned char *) a;
  unsigned char *y = (unsigned char *) b;
  return *x - *y;
}

unsigned char find_new_center(unsigned char* clusters,
    int nbClusters, unsigned char* elm)
{
  unsigned char min_clus = 0;
  int min = INT_MAX;
  __m64 b = {(elm[0] << 16) + (elm[1] << 8) + elm[2],
    (elm[3] << 8) + elm[4]};
  for (unsigned char i = 0; i < nbClusters; i++) {
    __m64 a = {(clusters[i * 5 + 0] << 16) + (clusters[i * 5 + 1] << 8) + clusters[i * 5 + 2],
      (clusters[i * 5 + 3] << 8) + clusters[i * 5 + 4]};
    int cur = _m_psadbw(a, b)[0];
    if (cur < min) {
      min = cur;
      min_clus = i;
    }
  }
  return min_clus;
}

void update_center(unsigned char* vsArr, int nbCol, int nbLin, 
    unsigned char* clusters, int nbClus) {
  __m128* means = calloc(nbClus, sizeof(__m128));
  long* mean_last = calloc(nbClus, sizeof(long));
  int* cmp = calloc(nbClus, sizeof(int));
  if (!means || !mean_last || !cmp)
    error(1, 0, "An error has occured during allocation");
  #pragma omp parallel
  {
    __m128* private_means = calloc(2 * nbClus, sizeof(__m128));
    long* private_mean_last = calloc(nbClus, sizeof(long));
    int* private_cmp = calloc(nbClus, sizeof(int));
    if (!private_means || !private_mean_last || !private_cmp)
      error(1, 0, "An error has occured during allocation");
    #pragma omp for 
    for (size_t i = 0; i < nbCol * nbLin ; i++)
    {
      __m128 vsFourFirstElm = {vsArr[i * 6 + 1], vsArr[i * 6 + 2], vsArr[i * 6 + 3], vsArr[i * 6 + 4]};
      private_means[vsArr[i*6]] = _mm_add_ps(private_means[vsArr[i*6]], vsFourFirstElm);
      private_mean_last[vsArr[i*6]] += vsArr[i*6 + 5];
      private_cmp[vsArr[i*6]] += 1;
    }

    #pragma omp critical
    for (int i = 0; i < nbClus; i++) {
      means[i] = _mm_add_ps(private_means[i], means[i]);
      mean_last[i] += private_mean_last[i];
      cmp[i] += private_cmp[i];
    }
    free(private_means);
    free(private_cmp);
    free(private_mean_last);

  }


  for (unsigned char i = 0; i < nbClus; i++) {
    if (cmp[i]) {
      means[i] = _mm_div_ps(means[i], _mm_set1_ps(cmp[i]));
      mean_last[i] /= cmp[i];
      for (unsigned char j = 0; j < 4; j++)
        *(clusters + i * 5 + j) = means[i][j];
      clusters[i * 5 + 4] = mean_last[i];
    }
  }

  int median = clusters[2];
  for (unsigned char i = 0; i < 5; i++)
    clusters[i] = median;

  free(means);
  free(mean_last);
  free(cmp);
}

unsigned char* init_clusters(int nbClus)
{
  unsigned char* clusters = malloc(sizeof(char) * 5 * nbClus);
 /* cluster that represents cloud is the first one */
  for (int j = 0; j < 5; j++)
    clusters[j] = 255;

  for (int i = 1; i < nbClus; i++)
  {
    unsigned char p = (nbClus - i - 1)*(255/nbClus); 
    for (int j = 0; j < 5; j++)
      *(clusters + i * 5 + j) = p;
  }
  return clusters;
}

unsigned char* create_vsArray(guchar *pucImaOrig, int nbLine, int nbCol,
    int iNbChannels)
{
  int nbTotalPix = nbLine * nbCol;
  unsigned char* vsArray = malloc(sizeof(unsigned char) * 6 * nbLine * nbCol);
  if (!vsArray)
    error(1, 0, "An error has occured during allocation");
  // iVsArray represent the spacialized vector
  # pragma omp parallel for
  for(int iNumPix=0;
      iNumPix<nbTotalPix*iNbChannels;
      iNumPix+=iNbChannels){
    // cluster index
    size_t indVsArray = iNumPix * 6 / iNbChannels;
    *(vsArray + indVsArray) = 0;
    *(vsArray + indVsArray + 1) = *(pucImaOrig + iNumPix + 1);
    if (iNumPix >= nbCol * iNbChannels)
      *(vsArray + indVsArray + 2) = *(pucImaOrig + iNumPix - nbCol * iNbChannels + 1);
    else
      *(vsArray + indVsArray + 2) = 255;
    if ((iNumPix + iNbChannels) % (nbCol*iNbChannels) != 0)
      *(vsArray + indVsArray + 3) = *(pucImaOrig + iNumPix + iNbChannels + 1);
    else 
      *(vsArray + indVsArray + 3) = 255;
    if (iNumPix % (nbCol*iNbChannels) != 0)
      *(vsArray + indVsArray + 4) = *(pucImaOrig + iNumPix - iNbChannels + 1);
    else
      *(vsArray + indVsArray + 4) = 255;
    if (iNumPix + nbCol * iNbChannels < nbTotalPix*iNbChannels)
      *(vsArray + indVsArray + 5) = *(pucImaOrig + iNumPix + nbCol * iNbChannels + 1);
    else
      *(vsArray + indVsArray + 5) = 255;
    qsort(vsArray + indVsArray + 1, 5, sizeof(unsigned char), compare_function);
  }
  return vsArray;
}

void SegmentCloudImage(guchar *pucImaOrig, 
		  int nbLine,
		  int nbCol, 
		  int panchro,
      guchar *pucImaRes)
{
  int nbClusters = panchro ? 9 : 8;
  int iNbChannels = 3;

  /* creation of Vs vector */
  unsigned char* vsArray = create_vsArray(pucImaOrig, nbLine, nbCol,
      iNbChannels);

  /* clusters initialisation */
  unsigned char* clusters = init_clusters(nbClusters);

  /* clusters update */
  int change = 1;
  while(change)
  {
    change = 0;
    #pragma omp parallel for reduction(||:change)
    for (size_t i = 0; i < nbCol * nbLine * 6; i += 6) {
      unsigned char prev = vsArray[i];
      unsigned char new = find_new_center(clusters, nbClusters,
          vsArray + i + 1);
      if (prev != new) {
        change = 1;
        vsArray[i] = new;
      }
    }
    update_center(vsArray, nbCol, nbLine, clusters, nbClusters);
  }
  
  
  #pragma omp parallel for
  for (size_t ind = 0; ind < nbCol * nbLine * 6; ind += 6) {
    if (vsArray[ind] == 0) {
      for (int i = 0; i < iNbChannels; i++)
        *(pucImaRes + (ind/6 * iNbChannels) + i) = 255;
    } else {
      for (int i = 0; i < iNbChannels; i++)
        *(pucImaRes + (ind/6 * iNbChannels) + i) = 0;
    }
  }
}

void ComputeImage(guchar *pucImaOrig, 
		  int NbLine,
		  int NbCol, 
		  guchar *pucImaRes)
{
  int iNbPixelsTotal, iNumPix;
  int iNbChannels=3; /* we work on colored images*/
  int panchro = 1;

  iNbPixelsTotal=NbCol*NbLine;

  /* determinisation of the panchromatic nature of the image */
  for(iNumPix=0;
      iNumPix<iNbPixelsTotal*iNbChannels;
      iNumPix=iNumPix+iNbChannels){
    if (*(pucImaOrig+iNumPix) != *(pucImaOrig+iNumPix + 1) ||
        *(pucImaOrig+iNumPix) != *(pucImaOrig+iNumPix + 2) ||
        *(pucImaOrig+iNumPix + 1) != *(pucImaOrig+iNumPix + 2)){
      panchro = 0;
      break;
    }
  }

  SegmentCloudImage(pucImaOrig, NbLine, NbCol, panchro, pucImaRes);
}
