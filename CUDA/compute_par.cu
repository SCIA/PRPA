#include <stdio.h>
#include <stdlib.h>
//#include <gtk/gtk.h>
#include <error.h>
#include <time.h>
#include <omp.h>
#include <xmmintrin.h>
#include <cuda.h>
#include <cuda_runtime.h>

#include "book.h"

/*******************************************************
IL EST FORMELLEMENT INTERDIT DE CHANGER LE PROTOTYPE
DES FONCTIONS
*******************************************************/

__host__ int compare_function(const void *a,const void *b) {
  unsigned char *x = (unsigned char *) a;
  unsigned char *y = (unsigned char *) b;
  return *x - *y;
}

__global__ void find_new_center_clusters(unsigned char* clusters, int nbClusters, unsigned char *elm, unsigned char *mins) {

  int i = blockIdx.x * blockDim.x + threadIdx.x;
  if (i < nbClusters) {
    long cur = 0;
    for (unsigned char j = 0; j < 5; j++) {
      int diff = clusters[i * 5 + j] - *(elm + j);
      diff = diff > 0 ? diff : -diff;
      cur = cur + diff;
    }
    *(mins + i) = cur;
  }
}
/*
unsigned char find_new_center(unsigned char* clusters,
    int nbClusters, unsigned char* elm)
{
  unsigned char min_clus = 0;
  int min = INT_MAX;
  for (unsigned char i = 0; i < nbClusters; i++) {
    long cur = 0;
    for (unsigned char j = 0; j < 5; j++) {
      int diff = clusters[i * 5 + j] - *(elm + j);
      diff = diff > 0 ? diff : -diff;
      cur = cur + diff;
    }
    if (cur < min) {
      min = cur;
      min_clus = i;
    }
  }
  return min_clus;
}*/

__global__ void update_center(unsigned char* vsArr, int nbCol, int nbLin, 
    unsigned char* clusters, int nbClus, long* means) {

  for (int i = 0; i < 6 * nbClus; i++) {
    means[i] = 0;
  }
  for (size_t i = 0; i < nbCol * nbLin * 6; i += 6)
  {
    for (unsigned char j = 1; j < 6; j++)
      *(means + vsArr[i] * 6 + j) += *(vsArr + i + j);
    *(means + vsArr[i] * 6) += 1;
  }

  //#pragma omp parallel
  for (unsigned char i = 0; i < nbClus; i++) {
    if (means[i*6])
      for (unsigned char j = 0; j < 5; j++)
        *(clusters + i * 5 + j) = *(means + i * 6 + j + 1) / *(means + i * 6);
  }

  int median = clusters[2];
  for (unsigned char i = 0; i < 5; i++)
    clusters[i] = median;
}

__host__ unsigned char* init_clusters(int nbClus)
{
  unsigned char* clusters = (unsigned char *)malloc(sizeof(unsigned char) * 5 * nbClus);
 /* la cluster cloud est le premier de ma list */
  for (int j = 0; j < 5; j++)
    clusters[j] = 255;

  for (int i = 1; i < nbClus; i++)
  {
    unsigned char p = (nbClus - i - 1)*(255/nbClus); 
    for (int j = 0; j < 5; j++)
      *(clusters + i * 5 + j) = p;
  }
  return clusters;
}

__host__ unsigned char* create_vsArray(unsigned char *pucImaOrig, int nbLine, int nbCol,
    int iNbChannels)
{
  int nbTotalPix = nbLine * nbCol;
  unsigned char* vsArray = (unsigned char *)malloc(sizeof(unsigned char) * 6 * nbLine * nbCol);
  if (!vsArray)
    error(1, 0, "An error has occured during allocation");
  unsigned char* iVsArray = vsArray;
  for(int iNumPix=0;
      iNumPix<nbTotalPix*iNbChannels;
      iNumPix=iNumPix+iNbChannels){
    // cluster index
    *(iVsArray) = 0;
    *(iVsArray + 1) = *(pucImaOrig + iNumPix + 1);
    if (iNumPix >= nbCol * iNbChannels)
      *(iVsArray + 2) = *(pucImaOrig + iNumPix - nbCol * iNbChannels + 1);
    else
      *(iVsArray + 2) = 255;
    if ((iNumPix + iNbChannels) % (nbCol*iNbChannels) != 0)
      *(iVsArray + 3) = *(pucImaOrig + iNumPix + iNbChannels + 1);
    else 
      *(iVsArray + 3) = 255;
    if (iNumPix % (nbCol*iNbChannels) != 0)
      *(iVsArray + 4) = *(pucImaOrig + iNumPix - iNbChannels + 1);
    else
      *(iVsArray + 4) = 255;
    if (iNumPix + nbCol * iNbChannels < nbTotalPix*iNbChannels)
      *(iVsArray + 5) = *(pucImaOrig + iNumPix + nbCol * iNbChannels + 1);
    else
      *(iVsArray + 5) = 255;
    qsort(iVsArray + 1, 5, sizeof(unsigned char), compare_function);
    iVsArray += 6;
  }
  return vsArray;
}

/*__globaddl__ void add_v(int a, int b, int* c) {
	*c = a + b;
}*/

__host__ void kmean_loop(unsigned char* pucImaOrig, int nbLine,
		      int nbCol, unsigned char* dev_clusters,
		      unsigned char* vsArray, int nbClusters,
		      unsigned char *pucImaRes) {
  
  int change = 1;
  int iNbChannels = 3;

  unsigned char mins[nbClusters];
  unsigned char *dev_mins;
  HANDLE_ERROR( cudaMalloc(&dev_mins, sizeof(unsigned char) * nbClusters));
  HANDLE_ERROR(cudaMemcpy(dev_mins, &mins, sizeof(unsigned char)*nbClusters, cudaMemcpyHostToDevice));

  //HANDLE_ERROR( cudaMalloc((void **)&dev_mins, sizeof(unsigned char) * nbClusters));
  long* means;
  HANDLE_ERROR( cudaMalloc((void **)&means, sizeof(long) * 6 * nbClusters));
  while(change)
  {
    change = 0;
    for (size_t i = 0; i < nbCol * nbLine * 6; i += 6) {
      unsigned char prev = vsArray[i];
      int blockSize = 256;
      int numBlocks = (nbClusters + blockSize - 1) / blockSize;
      
      find_new_center_clusters<<<numBlocks, blockSize>>>(dev_clusters, nbClusters,
          vsArray + i + 1, dev_mins);
      cudaDeviceSynchronize();
      HANDLE_ERROR(cudaMemcpy(&mins, dev_mins, sizeof(unsigned char)*nbClusters, cudaMemcpyDeviceToHost));
      int min_clus = 0;
      int min = INT_MAX;
      for (int u =0; u < nbClusters; u++) {
	if (mins[u] < min) {
	  min = mins[u];
	  min_clus = u;
	}
      }
      if (prev != min_clus) {
        change = 1;
        vsArray[i] = min_clus;
      }
    }
    update_center<<<1,1>>>(vsArray, nbCol, nbLine, dev_clusters, nbClusters, means);
    cudaDeviceSynchronize();
  }
  
  
  //#pragma omp parallel
  for (size_t ind = 0; ind < nbCol * nbLine * 6; ind += 6) {
    if (vsArray[ind] == 0) {
      for (int i = 0; i < iNbChannels; i++)
        *(pucImaRes + (ind/6 * iNbChannels) + i) = 255;
    } else {
      for (int i = 0; i < iNbChannels; i++)
        *(pucImaRes + (ind/6 * iNbChannels) + i) = 0;
    }
  }

}


__host__ void SegmentCloudImage(unsigned char *pucImaOrig, 
		       int nbLine,
		       int nbCol, 
		       int panchro,
		       unsigned char *pucImaRes)
{
  int nbClusters = panchro ? 9 : 8;
  int iNbChannels = 3;

  /* creation des vecteurs Vs */
  unsigned char* vsArray = create_vsArray(pucImaOrig, nbLine, nbCol,
      iNbChannels);
 /* unsigned char* dev_vsArray;
  cudaMalloc((void **)&dev_vsArray, sizeof(unsigned char)* 6 * nbCol * nbLine);
  cudaMemcpy(dev_vsArray, vsArray, sizeof(unsigned char) * 6 * nbCol * nbLine, cudaMemcpyHostToDevice);
*/
  /* initialisation de mes clusters */
  unsigned char* clusters = init_clusters(nbClusters);
  unsigned char* dev_clusters;
  cudaMalloc((void **)&dev_clusters, sizeof(unsigned char)* 5 * nbClusters);
  cudaMemcpy(dev_clusters, clusters, sizeof(unsigned char) * 5 * nbClusters, cudaMemcpyHostToDevice);

  unsigned char* dev_pucImaOrig;
  cudaMalloc((void **)&dev_pucImaOrig, sizeof(unsigned char) * 3 * nbLine * nbCol);
  cudaMemcpy(dev_pucImaOrig, pucImaOrig, sizeof(unsigned char) * 3 * nbLine * nbCol, cudaMemcpyHostToDevice);
  /*unsigned char* dev_pucImaRes;
  cudaMalloc((void **)&dev_pucImaRes, sizeof(unsigned char) * 3 * nbLine * nbCol);
  cudaMemcpy(dev_pucImaRes, pucImaRes, sizeof(unsigned char) * 3 * nbLine * nbCol, cudaMemcpyHostToDevice);
*/
  kmean_loop(dev_pucImaOrig, nbLine, nbCol, dev_clusters, vsArray, nbClusters, pucImaRes);



  //cudaMemcpy(pucImaRes, dev_pucImaRes, sizeof(unsigned char) * 3 * nbLine * nbCol, cudaMemcpyDeviceToHost);
  /* mise a jour de mes clusters */
/*  int change = 1;
  while(change)
  {
    change = 0;
    for (size_t i = 0; i < nbCol * nbLine * 6; i += 6) {
      unsigned char prev = vsArray[i];
      unsigned char new = find_new_center(clusters, nbClusters,
          vsArray + i + 1);
      if (prev != new) {
        change = 1;
        vsArray[i] = new;
      }
    }
    update_center(vsArray, nbCol, nbLine, clusters, nbClusters);
  }
  
  
  //#pragma omp parallel
  for (size_t ind = 0; ind < nbCol * nbLine * 6; ind += 6) {
    if (vsArray[ind] == 0) {
      for (int i = 0; i < iNbChannels; i++)
        *(pucImaRes + (ind/6 * iNbChannels) + i) = 255;
    } else {
      for (int i = 0; i < iNbChannels; i++)
        *(pucImaRes + (ind/6 * iNbChannels) + i) = 0;
    }
  }*/
}

/*---------------------------------------
  Proto: 

  
  But: 

  Entrees: 
          --->le tableau des valeurs des pixels de l'image d'origine
	      (les lignes sont mises les unes � la suite des autres)
	  --->le nombre de lignes de l'image,
	  --->le nombre de colonnes de l'image,
          --->le tableau des valeurs des pixels de l'image resultat
	      (les lignes sont mises les unes � la suite des autres)


  Sortie:

  Rem: 

  Voir aussi:

  ---------------------------------------*/
extern "C"
void ComputeImage(unsigned char *pucImaOrig, 
		  int NbLine,
		  int NbCol, 
		  unsigned char *pucImaRes)
{
  int iNbPixelsTotal, iNumPix;
  int iNbChannels=3; /* on travaille sur des images couleurs*/
  int panchro = 1;

  printf("HA");

  iNbPixelsTotal=NbCol*NbLine;

  clock_t start = clock();
  /* determinisation de la nature panchromatique de l'image */
  for(iNumPix=0;
      iNumPix<iNbPixelsTotal*iNbChannels;
      iNumPix += iNbChannels){
    __m128 pv1 = _mm_setr_ps(pucImaOrig[iNumPix], pucImaOrig[iNumPix + 1], pucImaOrig[iNumPix + 2], 0);
    __m128 pv2 = _mm_setr_ps(pucImaOrig[iNumPix + 2], pucImaOrig[iNumPix], pucImaOrig[iNumPix + 1], 0);
    if (!_mm_comieq_ss(pv1, pv2)) {
      panchro = 0;
      printf("panchro\n");
      break;
    }
  }
  printf("%d\n", clock() - start);
/*  unsigned char* dev_pucImaOrig;
  cudaMalloc((void **)&dev_pucImaOrig, sizeof(unsigned char) * 3 * NbLine * NbCol);
  cudaMemcpy(dev_pucImaOrig, pucImaOrig, sizeof(unsigned char) * 3 * NbLine * NbCol, cudaMemcpyHostToDevice);
  unsigned char* dev_pucImaRes;
  cudaMalloc((void **)&dev_pucImaRes, sizeof(unsigned char) * 3 * NbLine * NbCol);
  cudaMemcpy(dev_pucImaRes, pucImaRes, sizeof(unsigned char) * 3 * NbLine * NbCol, cudaMemcpyHostToDevice);
*/
  SegmentCloudImage(pucImaOrig, NbLine, NbCol, panchro, pucImaRes);
  //kmean_loop<<<1,1>>>(dev_pucImaOrig, NbLine, NbCol, panchro, dev_pucImaRes);
}
