#include <stdio.h>
#include <stdlib.h>
#include <gtk/gtk.h>
#include <error.h>

/*******************************************************
IL EST FORMELLEMENT INTERDIT DE CHANGER LE PROTOTYPE
DES FONCTIONS
*******************************************************/

int compare_function(const void *a,const void *b) {
  unsigned char *x = (unsigned char *) a;
  unsigned char *y = (unsigned char *) b;
  return *x - *y;
}

unsigned char find_new_center(unsigned char* clusters,
    int nbClusters, unsigned char* elm)
{
  unsigned char min_clus = 0;
  int min = INT_MAX;
  for (unsigned char i = 0; i < nbClusters; i++) {
    long cur = 0;
    for (unsigned char j = 0; j < 5; j++) {
      int diff = clusters[i * 5 + j] - *(elm + j);
      diff = diff > 0 ? diff : -diff;
      cur = cur + diff;
    }
    if (cur < min) {
      min = cur;
      min_clus = i;
    }
  }
  return min_clus;
}

void update_center(unsigned char* vsArr, int nbCol, int nbLin, 
    unsigned char* clusters, int nbClus) {
  long* means = calloc(6 * nbClus, sizeof(long));
  if (!means)
    error(1, 0, "An error has occured during allocation");
  for (size_t i = 0; i < nbCol * nbLin * 6; i += 6)
  {
    for (unsigned char j = 1; j < 6; j++)
      *(means + vsArr[i] * 6 + j) += *(vsArr + i + j);
    *(means + vsArr[i] * 6) += 1;
  }

  for (unsigned char i = 0; i < nbClus; i++) {
    if (means[i*6])
      for (unsigned char j = 0; j < 5; j++)
        *(clusters + i * 5 + j) = *(means + i * 6 + j + 1) / *(means + i * 6);
  }

  int median = clusters[2];
  for (unsigned char i = 0; i < 5; i++)
    clusters[i] = median;
}

unsigned char* init_clusters(int nbClus)
{
  unsigned char* clusters = malloc(sizeof(char) * 5 * nbClus);
 /* la cluster cloud est le premier de ma list */
  for (int j = 0; j < 5; j++)
    clusters[j] = 255;

  for (int i = 1; i < nbClus; i++)
  {
    unsigned char p = (nbClus - i - 1)*(255/nbClus); 
    for (int j = 0; j < 5; j++)
      *(clusters + i * 5 + j) = p;
  }
  return clusters;
}

unsigned char* create_vsArray(guchar *pucImaOrig, int nbLine, int nbCol,
    int iNbChannels)
{
  int nbTotalPix = nbLine * nbCol;
  unsigned char* vsArray = malloc(sizeof(unsigned char) * 6 * nbLine * nbCol);
  if (!vsArray)
    error(1, 0, "An error has occured during allocation");
  unsigned char* iVsArray = vsArray;
  for(int iNumPix=0;
      iNumPix<nbTotalPix*iNbChannels;
      iNumPix=iNumPix+iNbChannels){
    // cluster index
    *(iVsArray) = 0;
    *(iVsArray + 1) = *(pucImaOrig + iNumPix + 1);
    if (iNumPix >= nbCol * iNbChannels)
      *(iVsArray + 2) = *(pucImaOrig + iNumPix - nbCol * iNbChannels + 1);
    else
      *(iVsArray + 2) = 255;
    if ((iNumPix + iNbChannels) % (nbCol*iNbChannels) != 0)
      *(iVsArray + 3) = *(pucImaOrig + iNumPix + iNbChannels + 1);
    else 
      *(iVsArray + 3) = 255;
    if (iNumPix % (nbCol*iNbChannels) != 0)
      *(iVsArray + 4) = *(pucImaOrig + iNumPix - iNbChannels + 1);
    else
      *(iVsArray + 4) = 255;
    if (iNumPix + nbCol * iNbChannels < nbTotalPix*iNbChannels)
      *(iVsArray + 5) = *(pucImaOrig + iNumPix + nbCol * iNbChannels + 1);
    else
      *(iVsArray + 5) = 255;
    qsort(iVsArray + 1, 5, sizeof(unsigned char), compare_function);
    iVsArray += 6;
  }
  return vsArray;
}

void SegmentCloudImage(guchar *pucImaOrig, 
		  int nbLine,
		  int nbCol, 
		  int panchro,
      guchar *pucImaRes)
{
  int nbClusters = panchro ? 9 : 8;
  int iNbChannels = 3;

  /* creation des vecteurs Vs */
  unsigned char* vsArray = create_vsArray(pucImaOrig, nbLine, nbCol,
      iNbChannels);

  /* initialisation de mes clusters */
  unsigned char* clusters = init_clusters(nbClusters);

  /* mise a jour de mes clusters */
  int change = 1;
  while(change)
  {
    change = 0;
    for (size_t i = 0; i < nbCol * nbLine * 6; i += 6) {
      unsigned char prev = vsArray[i];
      unsigned char new = find_new_center(clusters, nbClusters,
          vsArray + i + 1);
      if (prev != new) {
        change = 1;
        vsArray[i] = new;
      }
    }
    update_center(vsArray, nbCol, nbLine, clusters, nbClusters);
  }
  
  
  for (size_t ind = 0; ind < nbCol * nbLine * 6; ind += 6) {
    if (vsArray[ind] == 0) {
      for (int i = 0; i < iNbChannels; i++)
        *(pucImaRes + (ind/6 * iNbChannels) + i) = 255;
    } else {
      for (int i = 0; i < iNbChannels; i++)
        *(pucImaRes + (ind/6 * iNbChannels) + i) = 0;
    }
  }
}

/*---------------------------------------
  Proto: 

  
  But: 

  Entrees: 
          --->le tableau des valeurs des pixels de l'image d'origine
	      (les lignes sont mises les unes � la suite des autres)
	  --->le nombre de lignes de l'image,
	  --->le nombre de colonnes de l'image,
          --->le tableau des valeurs des pixels de l'image resultat
	      (les lignes sont mises les unes � la suite des autres)


  Sortie:

  Rem: 

  Voir aussi:

  ---------------------------------------*/
void ComputeImage(guchar *pucImaOrig, 
		  int NbLine,
		  int NbCol, 
		  guchar *pucImaRes)
{
  int iNbPixelsTotal, iNumPix;
  int iNbChannels=3; /* on travaille sur des images couleurs*/
  int panchro = 1;

  iNbPixelsTotal=NbCol*NbLine;

  /* determinisation de la nature panchromatique de l'image */
  for(iNumPix=0;
      iNumPix<iNbPixelsTotal*iNbChannels;
      iNumPix=iNumPix+iNbChannels){
    if (*(pucImaOrig+iNumPix) != *(pucImaOrig+iNumPix + 1) ||
        *(pucImaOrig+iNumPix) != *(pucImaOrig+iNumPix + 2) ||
        *(pucImaOrig+iNumPix + 1) != *(pucImaOrig+iNumPix + 2)){
      panchro = 0;
      break;
    }
  }


  /* moyennage de l'image */
  /*for(iNumPix=0;
      iNumPix<iNbPixelsTotal*iNbChannels;
      iNumPix=iNumPix+iNbChannels){
    int sum = (*(pucImaOrig+iNumPix) + *(pucImaOrig+iNumPix + 1) + *(pucImaOrig+iNumPix + 2)) / 3;
    *(pucImaOrig+iNumPix) = sum;
    *(pucImaOrig+iNumPix + 1) = sum;
    *(pucImaOrig+iNumPix + 2) = sum;
  }*/

  /* segmentation de l'image */
  SegmentCloudImage(pucImaOrig, NbLine, NbCol, panchro, pucImaRes);
}
