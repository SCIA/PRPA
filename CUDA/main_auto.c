#include <dirent.h>
#include <gtk/gtk.h>
#include <mpi.h>
#include <omp.h>
#include <stdlib.h>
#include <string.h>

#include "compute_par.h"

struct info_t {
  int nproc;
  int rang;

  int ntot;
  int nloc;

  int ideb;
  int ifin;
};

void init_info(int n, struct info_t *info) {

  int Q, R;

  MPI_Comm_rank(MPI_COMM_WORLD, &(info->rang));
  MPI_Comm_size(MPI_COMM_WORLD, &(info->nproc));

  Q = n / info->nproc;
  R = n % info->nproc;

  info->ntot = n;

  if (info->rang < R) {

    info->nloc = Q+1;
    info->ideb = info->rang * (Q+1);
    info->ifin = info->ideb + info->nloc;

  } else {

    info->nloc = Q;
    info->ideb = R * (Q+1) + (info->rang - R) * Q;
    info->ifin = info->ideb + info->nloc;
  }

  printf("%d %d %d %d %d %d\n", info->nproc, info->rang, info->ntot, info->nloc, info->ideb, info->ifin);
}

void send_parallel(unsigned char* pucImaOrig, int nbLine, int nbCol, int nbProc, char* file_name){
  MPI_Send(file_name, strlen(file_name) + 1, MPI_INT, nbProc, nbProc, MPI_COMM_WORLD);
  MPI_Send(&nbLine, 1, MPI_INT, nbProc, nbProc, MPI_COMM_WORLD);
  MPI_Send(&nbCol, 1, MPI_INT, nbProc, nbProc, MPI_COMM_WORLD);
  MPI_Send(pucImaOrig, nbLine * nbCol * 3, MPI_UNSIGNED_CHAR, nbProc, nbProc, MPI_COMM_WORLD);
} 

void read_parallel(int nbProc) {
  for (int i = 0; i < nbProc; i++) {
    MPI_Status stat;
    double res;
    char file[255];
    MPI_Recv(file, 255, MPI_CHAR, MPI_ANY_SOURCE, 0, MPI_COMM_WORLD, &stat);
    MPI_Recv(&res, 1, MPI_DOUBLE, MPI_ANY_SOURCE, 0, MPI_COMM_WORLD, &stat);
    printf("%s %f\n", file, res);
  }
}


int computeOne(struct info_t info) {
  MPI_Status stat;
  char file[255];
  MPI_Recv(&file, 255, MPI_CHAR, 0, info.rang, MPI_COMM_WORLD, &stat);
  if (strncmp(file, "stop", 4) == 0)
    return 0;
  int nbLine, nbCol;
  MPI_Recv(&nbLine, 1, MPI_INT, 0, info.rang, MPI_COMM_WORLD, &stat);
  MPI_Recv(&nbCol, 1, MPI_INT, 0, info.rang, MPI_COMM_WORLD, &stat);
  unsigned char* pucImaOrig = malloc(nbLine * nbCol * 3 * sizeof(unsigned char));
  MPI_Recv(pucImaOrig, nbLine*nbCol * 3, MPI_UNSIGNED_CHAR, 0, info.rang, MPI_COMM_WORLD, &stat);
  ComputeImage((unsigned char*)pucImaOrig, nbLine, nbCol, (unsigned char*)pucImaOrig);
  int white = 0;
  for (int i = 0; i < nbLine * nbCol * 3; i = i + 3)
    if (*(pucImaOrig + i) == 255)
      white++;
  double res = (white/((double) (nbCol * nbLine)) * 100);
  MPI_Send(file, strlen(file) + 1, MPI_CHAR, 0, 0, MPI_COMM_WORLD);
  MPI_Send(&res, 1, MPI_DOUBLE, 0, 0, MPI_COMM_WORLD);
  return 1;
}

void send_end(int nbProc) {
  for (int i = 0; i < nbProc; i++) {
    MPI_Send("stop", 5, MPI_CHAR, i + 1, i + 1, MPI_COMM_WORLD);
  }
}

int main(int argc, char **argv) {
  struct info_t info;

  MPI_Init(&argc, &argv);

  init_info(0, &info);

  int nbProc = info.nproc;

  if (info.rang == 0) {
    DIR *d;
    struct dirent *dir;
    d = opendir(".");
    if (d) {
      while ((dir = readdir(d)) != NULL) {
        int size = strlen(dir->d_name);
        if (dir->d_type == DT_REG && (size >= 4 &&
              strcmp((dir->d_name + size - 4), ".exe"))){
          GdkPixbuf* pGdkPix = gdk_pixbuf_new_from_file(dir->d_name, NULL);
          guchar* pucImaOrig = gdk_pixbuf_get_pixels(pGdkPix);
          int nbLine = gdk_pixbuf_get_height(pGdkPix);
          int nbCol = gdk_pixbuf_get_width(pGdkPix);
          int nbChannel = 3;
          if (info.nproc == 1) {
            double bef = omp_get_wtime();
            ComputeImage((unsigned char *)pucImaOrig, nbLine, nbCol, (unsigned char *)pucImaOrig);
            int white = 0;
            for (int i = 0; i < nbLine * nbCol * nbChannel; i = i + nbChannel)
              if (*(pucImaOrig + i) == 255)
                white++;

            printf("%f %s : %f\n", omp_get_wtime() - bef, dir->d_name,
                (white/((double) (nbCol * nbLine)) * 100));
          } else {
            if (nbProc == 1) {
              read_parallel(info.nproc - nbProc);
              nbProc = info.nproc;
            }
            send_parallel(pucImaOrig, nbLine, nbCol, --nbProc, dir->d_name);
          }
        }
      }
      read_parallel(info.nproc - nbProc);
      send_end(info.nproc - nbProc);
      closedir(d);
    }
    else {
      printf("error while reading the searching for files\n");
      return 1;
    }
  } else {
    while (1)
      if (!computeOne(info))
        break;
  }
  MPI_Finalize();
  return 0;
}
